const express = require("express");
const mongoose = require("mongoose");
const userAccountRoutes = require("./routes/userAccountRoutes");
// const courseRoutes = require("./routes/courseRoutes");

// Allows our backend application to be available with our frontend application.
// Cross Origin Resource Sharing
const cors = require("cors");

const app = express();



// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.o8syqdi.mongodb.net/ecommerceTestAPI?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));



// middlewares
/* 
Allow all resources to access our backend application
*/
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/* 
Defines the "/users" string to be included for all user routes in the "userRoutes" file
*/
app.use("/useraccounts", userAccountRoutes);


// "/courses" string will be included for all course routes inside the "courseRoutes" file.
// app.use("/courses", courseRoutes);



/* 
This syntax will allow flixibility when using the application locally or as a hosted app
*/

const port = process.env.PORT || 4000


app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
});
