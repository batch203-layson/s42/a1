const mongoose = require("mongoose");

const userAccountSchema = new mongoose.Schema({
    userAccountFirstName: {
        type: String,
        required: [true, "First name is required"]
    },
    userAccountLastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    userAccountEmail: {
        type: String,
        required: [true, "Email is required"]
    },
    userAccountPassword: {
        type: String,
        required: [true, "Password is required"]
    },
    userAccountMobileNumber: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    userAccountiSAdmin: {
        type: Boolean,
        default: false
    },
    userAccountCreateOn: {
        type: Date,
        default: new Date()
    },
    requestOrders: [
        {
            requestOrdersTotalAmount: {
                type: Number,
                required: [true, "Orders Total Amount is required"]
            },
            requestOrdersPurchaseDate: {
                type: Date,
                default: new Date()
            },
            productItems: [
                {
                productItemId: {
                    type: String,
                        default: "Product/Item ID is required"
                    },
                productItemQuantity: {
                    type: Number,
                    default: "Product/Item quantity is required"
                    }
                }
            ]
        }
    ]
})

module.exports = mongoose.model("UserAccount", userAccountSchema);