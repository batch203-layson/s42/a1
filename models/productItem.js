const mongoose = require("mongoose");

const productItemSchema = new mongoose.Schema({
    productItemName: {
        type: String,
        required: [true, "Product/Item name is required"]
    },
    productItemtDescription: {
        type: String,
        required: [true, "Product/Item description is required"]
    },
    productItemPrice: {
        type: String,
        required: [true, "Product/Item price is required"]
    },
    productItemStocks: {
        type: Number,
        required: [true, "Product/Item number of stocks is required"]
    },
    productItemIsActive: {
        type: Boolean,
        default: true
    },
    productItemSetUpOn: {
        type: Date,
        default: new Date()
    },
    requestOrders: [
        {
            productItemId: {
                type: String,
                required: [true, "Product/Item ID is required"]
            },
            userAccountId: {
                type: String,
                required: [true, "Product/Item ID is required"]
            },
            productItemQuantity: {
                type: Number,
                required: [true, "Product/Item ID is required"]
            },
            requestOrdersPurchaseDate: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("ProductItem", productItemSchema);